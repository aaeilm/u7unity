﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Util {
    public static Vector3 Vec3Recip(Vector3 vec) {
        return new Vector3(1/vec.x, 1/vec.y, 1/vec.z);
    }

    public static Vector3 Vec3Mult(Vector3 a, Vector3 b) {
        return new Vector3(a.x*b.x, a.y*b.y, a.z*b.z);
    }

    public static T RandIn<T>(List<T> lst) {
        return lst[Random.Range(0, lst.Count)];
    }

    public static Vector3 PointInPlane(Transform t) {
        float x = Random.Range(t.position.x - 5, t.position.x + 5);
        float z = Random.Range(t.position.z - 5, t.position.z + 5);
        return new Vector3(x, 0, z);
    }
}

// todo base verticall offset for capsules

public class TankFloat : MonoBehaviour {
    public GameObject tank;

    public float tank_scl = 1;      // scl down for tank
    public float tank_v_offset = 0; // vertical offset
    public float begin_rot = 180;

    public float rot_spd;  // noise
    public float vert_spd = 0.5f; // sin
    public float rot_amt;
    public float vert_amt = 0.5f;

    float rot_acc;
    float vert_acc;
    
	void Start () {
        if (tank) {
            Init();
        }
	}

    public void Init () {
        this.transform.parent = tank.transform;
        this.transform.localPosition = Vector3.zero;
        this.transform.localEulerAngles = new Vector3(-90, 180, 0);
        this.transform.Rotate(new Vector3(0, begin_rot, 0));
        this.transform.localScale = tank_scl * Util.Vec3Mult(this.transform.lossyScale, Util.Vec3Recip(tank.transform.lossyScale));
        vert_acc = Random.Range(0.0f, Mathf.PI * 2);
        rot_acc = Random.Range(0, 100);
        vert_spd += Random.Range(-vert_spd/2, vert_spd/2);
        rot_spd += Random.Range(-rot_spd/2, rot_spd/2);
    }
	
	void Update () {
        rot_acc += rot_spd * Time.deltaTime;
        vert_acc += vert_spd * Time.deltaTime;

        this.transform.localPosition =
            new Vector3(
                this.transform.localPosition.x ,
                this.transform.localPosition.y ,
                tank_v_offset + Mathf.Sin(vert_acc) * vert_amt
                );

        this.transform.Rotate(
            new Vector3(
                0 ,
                Mathf.PerlinNoise(rot_acc, 0) * rot_amt - rot_amt/2 ,
                0
                )
            );
	}
}
