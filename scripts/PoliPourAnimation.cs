﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoliPourAnimation : MonoBehaviour {
    public GameObject pour_obj;
    public GameObject splash;
    public Transform splash_at;
    public float delay = 0.25f;

    GameObject last_splash;

    public void Pour(int is_on) {
        if (is_on == 0) {
            StartCoroutine(fn(false));
            Destroy(last_splash);
            last_splash = Instantiate(splash, splash_at);
        } else {
            StartCoroutine(fn(true));
        }
    }

    IEnumerator fn(bool tf) {
        yield return new WaitForSeconds(delay);
        pour_obj.SetActive(tf);
    }
}