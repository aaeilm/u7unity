﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimSetter : MonoBehaviour {
    public string trig;

    Animator anim;

    string last;

	void Start () {
        anim = this.GetComponent<Animator>();
        anim.SetTrigger(trig);
	}

    void Update() {
        if (trig != last) {
            anim.SetTrigger(trig);
        }

        last = trig;
    }
}