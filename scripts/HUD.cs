﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUD : MonoBehaviour {
    public float speed;
    public List<Texture> textures;
    public Light lite;
    public float b_low;
    public float b_high;

    Renderer screen;
    int tx_at = 0;
    float s_acc = 0;

    List<float> brightnesses;

	void Start () {
        screen = this.GetComponent<Renderer>();
        if (textures.Count > 1) {
            screen.material.mainTexture = textures[tx_at];

            brightnesses = new List<float>();
            for (int i = 0; i < textures.Count; ++i) {
                brightnesses.Add(Random.Range(b_low, b_high));
            }
        }
	}

	void Update () {
		s_acc += Time.deltaTime;

        if (textures.Count > 1 && s_acc > speed) {
            tx_at += 1;
            if (tx_at >= textures.Count) {
                tx_at = 0;
            }
            screen.material.mainTexture = textures[tx_at];
            s_acc = 0;

            lite.intensity = brightnesses[tx_at];
        }
	}
}