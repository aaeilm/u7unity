﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct PoliProp {
    public GameObject prop;
    public bool on;
}

public class PoliProps : MonoBehaviour {
    public List<PoliProp> props;

	void Start () {
		foreach (PoliProp p in props) {
            p.prop.SetActive(p.on);
        }
	}
}