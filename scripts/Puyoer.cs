﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Puyoer : MonoBehaviour {
    public Transform spawn_in;
    public List<string> move_anims;
    public List<string> rest_anims;

    public float rest_time_min;
    public float rest_time_max;

    NavMeshAgent agent;
    Animator anim;
    bool resting;

	void Start () {
		agent = GetComponent<NavMeshAgent>();
		anim = GetComponent<Animator>();
        agent.destination = Util.PointInPlane(spawn_in);
        anim.SetTrigger(Util.RandIn(move_anims));
	}
	
	void Update () {
        if (agent.remainingDistance == 0 && !resting) {
            StartCoroutine(Rest());
        }
	}

    IEnumerator Rest() {
        anim.SetTrigger(Util.RandIn(rest_anims));
        resting = true;
        yield return new WaitForSeconds(Random.Range(rest_time_min, rest_time_max));

        agent.destination = Util.PointInPlane(spawn_in);
        anim.SetTrigger(Util.RandIn(move_anims));
        resting = false;
    }
}