﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recorder : MonoBehaviour {
    public int frame_rate = 24;
    public bool do_record;

    public bool use_render_texture;

    public List<Camera> cameras;
    public int camera_sel;

    Camera curr_cam;

    string save_folder = "rec/";
    string rec_dir = "";

    bool last_do_record;
    float frame_acc;
    float frame_time;

    int frame_at;
    int rec_ct;

    int last_camera_sel;

    RenderTexture render_to;
    bool _use_rt;

    void Awake () {
        // QualitySettings.vSyncCount = 0;
        // Application.targetFrameRate = 24;
    }

    void Start () {
        _use_rt = use_render_texture;

        if (_use_rt)
            render_to = new RenderTexture(Screen.width, Screen.height, 32, RenderTextureFormat.ARGB32);
        
        frame_time = 1 / (float)frame_rate;
        set_camera(camera_sel);

        StartCoroutine(Screenshot());
    }

    IEnumerator Screenshot () {
        while (true) {

            yield return new WaitForEndOfFrame();

            if (do_record && !last_do_record) {
                start_record();
            } else if (!do_record && last_do_record) {
                end_record();
            }
            last_do_record = do_record;

            if (camera_sel != last_camera_sel) {
                set_camera(camera_sel);
            }

            last_camera_sel = camera_sel;

            frame_acc += Time.deltaTime;

            // Debug.Log(string.Format("{0}", Time.deltaTime));
        
            if (do_record && frame_acc > frame_time) {
                if (_use_rt) {
                    RenderTexture prev = RenderTexture.active;
                    RenderTexture.active = render_to;

                    int w = Screen.width;
                    int h = Screen.height;
                    Texture2D t = new Texture2D(w, h, TextureFormat.ARGB32, false);
                    t.ReadPixels(new Rect(0, 0, w, h), 0, 0);
                    t.Apply();
                    byte[] png = t.EncodeToPNG();
                    Destroy(t);
                    File.WriteAllBytes(get_filename(), png);

                    RenderTexture.active = prev;
                } else {
                    Application.CaptureScreenshot(get_filename());
                }

                frame_at += 1;
                frame_acc = 0;
            }

        }
    }

    void start_record() {
        rec_ct = int.Parse(File.ReadAllText(save_folder + "/rec_ct.txt"));
        rec_dir = string.Format("{0:D8}/", rec_ct);
        Directory.CreateDirectory(string.Format(save_folder + rec_dir));
        Debug.Log(string.Format("start record #{0}", rec_ct));
    }

    void end_record() {
        Debug.Log(string.Format("end record #{0}", rec_ct));
        rec_ct += 1;
        frame_at = 0;
        File.WriteAllText(save_folder + "/rec_ct.txt", string.Format("{0:D8}", rec_ct));
    }

    void set_camera(int to) {
        if (curr_cam) {
            curr_cam.gameObject.SetActive(false);
        }

        if (to >= 0 && to < cameras.Count) {
            curr_cam = cameras[to];
            curr_cam.gameObject.SetActive(true);
            curr_cam.targetTexture = render_to;
            camera_sel = to;
        }
    }

    string get_filename() {
        return save_folder + rec_dir + string.Format("{0}", frame_at) + ".png";
    }
}