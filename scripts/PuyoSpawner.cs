﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PuyoSpawner : MonoBehaviour {
    public int ct;
    public Transform plane_area;
    public List<GameObject> puyos;

    public float min_wait;
    public float max_wait;

    public float scl;

    public float speed = 1;

	void Start () {
        for (int i = 0; i < ct; ++i) {
            GameObject p = Instantiate(Util.RandIn(puyos)
                , Util.PointInPlane(plane_area)
                , Quaternion.identity);
            p.transform.parent = this.transform;
            p.transform.localScale = new Vector3(scl, scl, scl);

            Puyoer pr = p.GetComponent<Puyoer>();
            Debug.Assert(pr != null && "puyo game object must have puyoer script" != null);
            pr.spawn_in = plane_area;
            pr.rest_time_min = min_wait;
            pr.rest_time_max = max_wait;

            NavMeshAgent nma = p.GetComponent<NavMeshAgent>();
            nma.speed = speed;
        }
	}
}