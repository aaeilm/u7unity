﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterFloat : MonoBehaviour {
    public float vert_amt, vert_spd;

    float origin;
    float vert_acc;

	void Start () {
	    origin = this.transform.position.y;	
	}
	
	void Update () {
        vert_acc += vert_spd * Time.deltaTime;
	    this.transform.localPosition =
            new Vector3(
                this.transform.localPosition.x ,
                origin + Mathf.Sin(vert_acc) * vert_amt ,
                this.transform.localPosition.z
                );
	}
}