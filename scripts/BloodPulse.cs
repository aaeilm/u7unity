﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodPulse : MonoBehaviour {
    public List<Renderer> renderers;
    public Color low;
    public Color high;
    public float speed;
    public AnimationCurve curve;

    void Start() {
        foreach (Renderer rend in renderers) {
            rend.material.EnableKeyword("_EMISSION");
        }
    }

	float acc = 0;
	void Update () {
        acc += speed;
        float at = curve.Evaluate(acc);
        Color mix = low * (1 - at) + high * at;
        foreach (Renderer rend in renderers) {
            rend.material.SetColor("_EmissionColor", mix);
        }
	}
}