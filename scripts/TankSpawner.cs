﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct TankPuyo {
    public GameObject puyo;
    public float v_offset;
    public float v_amt;
    public float begin_rot;
    public float scl;
}

public class TankSpawner : MonoBehaviour {
    public List<GameObject> tanks;
    public List<TankPuyo> puyos;

    public float v_spd;
    public float r_spd;
    public float r_amt;

	void Start () {
        foreach (GameObject tank in tanks) {
            TankPuyo p = Util.RandIn<TankPuyo>(puyos);
            GameObject go = Instantiate(p.puyo);
            TankFloat tf = go.AddComponent<TankFloat>();

            tf.tank = tank;
            tf.vert_spd = v_spd;
            tf.vert_amt = p.v_amt;
            tf.rot_spd = r_spd;
            tf.rot_amt = r_amt;
            tf.tank_v_offset = p.v_offset;
            tf.begin_rot = p.begin_rot;
            tf.tank_scl = p.scl;

            tf.Init();
        }
	}
}